<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Task;
use App\TaskUser;

class AddTaskController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $taskDate = $request->date;
        $task = $request->task;
        $users = explode(',', $request->users);

        $tabTask = Task::create([
           'task_date'=>$taskDate,
           'task'=>$task
        ]);

        foreach($users as $user) {
            $tabTaskUser = TaskUser::create([
                'user'=>$user,
                'id_task'=>$tabTask->id
            ]);
        }
        return 'add task';
    }
}
