<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\TaskUser;

class TaskCalendarController extends Controller
{
    public function index()
    {
        $today = time();
        $weekday = date('N',$today);
        $day_sec = 60*60*24;
        $day = date('d' ,$today - ($weekday-1) * $day_sec);
        $week = date('m' ,$today - ($weekday-1) * $day_sec);
        $year = date('Y' ,$today - ($weekday-1) * $day_sec);

        for($count_day = 0; $count_day <= 6; $count_day++) {
            $bild_week = mktime(0, 0, 0, $week, $day+$count_day, $year);
            $now_date = date('Y-m-d',$bild_week);
            $tabTask = Task::select()->where('task_date',$now_date)->get();
            if(count($tabTask) == 0) {
                $arrData[$now_date] = null;
            }
            foreach($tabTask as $arrTasks) {
                $user_id = $arrTasks->id;
                $tabTaskUser = TaskUser::select()->where('id_task',$user_id)->get();
                foreach($tabTaskUser as $key => $arrUsers) {
                    $arrData[$now_date][$user_id][$arrUsers->id] = $arrUsers->user;
                }
            }
        }
        $row = 1;
        foreach($arrData as $arrDate) {
            if($arrDate != null) {
                if($row < count($arrDate)) {
                    $row = count($arrDate);
                }
            }
        }
        $weekdays = ['#','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        return view('index', compact('arrData','weekdays','row'));
    }
}
