<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskUser extends Model
{
    protected $table = 'tasks-users';
    protected $primaryKey = 'id';

    protected $fillable = ['user','id_task'];
}
