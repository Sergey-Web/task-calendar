<?php

use Illuminate\Database\Seeder;

class AddTasksUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            ['task'=>'Create table1 in DB','task_date'=>'2016-11-08'],
            ['task'=>'Create row in DB','task_date'=>'2016-11-08'],
            ['task'=>'Create table2 in DB','task_date'=>'2016-11-09'],
            ['task'=>'Create column in DB','task_date'=>'2016-11-10'],
            ['task'=>'Create column in DB','task_date'=>'2016-11-11'],
            ['task'=>'Create column in DB','task_date'=>'2016-11-12'],
            ['task'=>'Create column in DB','task_date'=>'2016-11-12'],
            ['task'=>'Create column in DB','task_date'=>'2016-11-12'],
        ]);
        DB::table('tasks-users')->insert([
            ['user'=>'Sergey','id_task'=>1],
            ['user'=>'Bogdan','id_task'=>1],
            ['user'=>'Leo   ','id_task'=>1],
            ['user'=>'Sergey','id_task'=>2],
            ['user'=>'Leo','id_task'=>2],
            ['user'=>'Bogdan','id_task'=>3],
            ['user'=>'Sergey','id_task'=>5],
            ['user'=>'Leo','id_task'=>4],
            ['user'=>'Bogdan','id_task'=>7],
            ['user'=>'Sergey','id_task'=>6],
            ['user'=>'Leo','id_task'=>8],
        ]);
    }
}