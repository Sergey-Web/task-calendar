$(function(){
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var _date = button.data('date');
        $(this).find('.task-date').html(_date);
    });

    $('#btnTask').on('click',function() {
        var users = [];
        $('.select2-selection__rendered > .select2-selection__choice').attr('title', function(key, val){
            users.push(val);
        });
        var task = $('#taskDescription').val();
        var _date = $('.task-date').text();

        var dataStr = 'date='+_date+'&task='+task+'&users='+[users];

        $.ajax({
           type: "POST",
            url: "addTask",
            data: dataStr,
            success: function(res) {
                console.log(res);
            }
        });

        return false;
    });
});