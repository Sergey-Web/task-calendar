@extends('layout')

@section('calendar')
    <table class="table table-bordered">
        <thead class="table-inverse">
            <tr>
                @foreach($weekdays as $weekday)
                    <td>{{$weekday}}</td>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @for($count = 1; $count <= $row+1; $count++)
                <tr>
                    <td>{{$count}}</td>
                    @foreach($arrData as $keyArrDate=>$arrDate)
                        @if($count == $row+1)
                            <td>
                                <a href="#" class="btn btn-primary" data-toggle="modal"
                                   data-target=".bs-example-modal-lg" data-date="{{$keyArrDate}}">
                                    ADD TASK
                                </a>
                            </td>
                        @else
                            @if($arrDate != null)
                                @if(array_slice($arrDate,$count-1,$count))
                                    <td>
                                    @foreach(array_slice($arrDate,$count-1,$count)[0] as $taskUser)
                                        @if(count(array_slice($arrDate,$count-1,$count)[0]) > 1)
                                                @foreach(array_keys($arrDate) as $keyIdTask => $idTask)
                                                    @if($keyIdTask+1 == $count)
                                                        {{($taskUser)}} ({{$idTask}})<br>
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach(array_keys($arrDate) as $keyIdTask => $idTask)
                                                    @if($keyIdTask+1 == $count)
                                                        {{($taskUser)}} ({{$idTask}})
                                                    @endif
                                                @endforeach
                                            @endif
                                    @endforeach
                                    </td>
                                @else
                                    <td></td>
                                @endif
                            @else
                                <td></td>
                            @endif
                        @endif
                    @endforeach
                </tr>
            @endfor
        </tbody>
    </table>
@endsection

@section('modal')
    <div id="exampleModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="modal-content__form" action="" method="post">
                    <div class="form-group form-group__custom">
                        <label for="taskDescription"><h2 class="form-group__title">Task <span class="task-date"></span></h2></label>
                        <textarea class="form-control form-control__custom" rows="5"name="task" id="taskDescription"></textarea>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <select class="js-example-basic-multiple" multiple="multiple">
                            <option>Sergey</option>
                            <option>Bogdan</option>
                            <option>Leo</option>
                        </select>

                        <script type="text/javascript">
                            $(".js-example-basic-multiple").select2();
                        </script>

                        <button id="btnTask" type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection