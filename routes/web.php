<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses'=>'TaskCalendarController@index', 'as'=>'calendar']);
/*Route::post('addTask', function() {
    if(Request::ajax()) {
        return Response::json(Request::all());
    }
});*/
Route::post('addTask', ['uses'=>'AddTaskController@index', 'as'=>'addTask']);
